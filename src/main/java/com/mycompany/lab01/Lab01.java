/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab01;

import java.util.Scanner;

/**
 *
 * @author sirikon
 */
public class Lab01 {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.println("Welcome to XOXO Game!");
        char[][] g = {{' ', ' ', ' '},
                      {' ', ' ', ' '},
                      {' ', ' ', ' '}};
        printG(g);
    
        while (true){
            PlayerO(g,kb);
            PlayerX(g,kb);
            if(FinishX(g)){
                break;
            }
        }
    }
        
        private static boolean FinishX(char[][] g){
        if((g[0][0] == 'X' && g[0][1] == 'X' && g[0][2] == 'X') ||
            (g[1][0] == 'X' && g[1][1] == 'X' && g[1][2] == 'X') ||
            (g[2][0] == 'X' && g[2][1] == 'X' && g[2][2] == 'X') ||
                
            (g[0][0] == 'X' && g[1][0] == 'X' && g[2][0] == 'X') ||
            (g[0][1] == 'X' && g[1][1] == 'X' && g[2][1] == 'X') ||
            (g[0][2] == 'X' && g[1][2] == 'X' && g[2][2] == 'X') ||

            (g[0][0] == 'X' && g[1][1] == 'X' && g[2][2] == 'X') ||
            (g[0][2] == 'X' && g[1][1] == 'X' && g[2][0] == 'X')){
            printG(g);
            System.out.println("Bingo! You wins!");
            return true;
    }
    
        for(int i=0; i<g.length; i++){
            for(int j=0; j<g[i].length; j++){
                if(g[i][j] == ' '){
                    return false;
                }
            }
        }
        printG(g);
        System.out.println("The End!");
        return true;
       
    }
        
        private static void printG(char[][] g){
        System.out.println(g[0][0]+"|"+g[0][1]+"|"+g[0][2]);
        System.out.println("-+-+-");
        System.out.println(g[1][0]+"|"+g[1][1]+"|"+g[1][2]);
        System.out.println("-+-+-");
        System.out.println(g[2][0]+"|"+g[2][1]+"|"+g[2][2]);
        }
        
       
        private static void PlayerO(char[][] g, Scanner kb){
            System.out.println("Turn O\nPlease select a number (1-9)");
            String num1 = kb.nextLine();
            switch(num1){
                case "1":
                    g[0][0] = 'O';
                    break;
                case "2":
                    g[0][1] = 'O';
                    break;
                case "3":
                    g[0][2] = 'O';
                    break;
                case "4":
                    g[1][0] = 'O';
                    break;
                case "5":
                    g[1][1] = 'O';
                    break;
                case "6":
                    g[1][2] = 'O';
                    break;
                case "7":
                    g[2][0] = 'O';
                    break;
                case "8":
                    g[2][1] = 'O';
                    break;
                case "9":
                    g[2][2] = 'O';
                    break;
        }

        printG(g);
    }
        
        private static void PlayerX(char[][] g, Scanner kb){
            System.out.println("Turn X\nPlease select a number (1-9)");
            String num2 = kb.nextLine();
            switch(num2){
                case "1":
                    g[0][0] = 'X';
                    break;
                case "2":
                    g[0][1] = 'X';
                    break;
                case "3":
                    g[0][2] = 'X';
                    break;
                case "4":
                    g[1][0] = 'X';
                    break;
                case "5":
                    g[1][1] = 'X';
                    break;
                case "6":
                    g[1][2] = 'X';
                    break;
                case "7":
                    g[2][0] = 'X';
                    break;
                case "8":
                    g[2][1] = 'X';
                    break;
                case "9":
                    g[2][2] = 'X';
                    break;
        }

        printG(g);
    }
        
    } 


